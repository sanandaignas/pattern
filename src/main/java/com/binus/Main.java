package com.binus;

public class Main {
    public static void printTriangleStar1(int n) {
        for(int i=0; i<n; i++){
            for(int j=0; j<=i; j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
    public static void printTriangleStar2 (int n){
        for(int a=0; a<n; a++){
            for(int b=0; b<a; b++){
                System.out.print(" ");
            }
            for(int c=n; c>a; c--){
                System.out.print("*");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        printTriangleStar1(3);
        System.out.println(" ");
        printTriangleStar2(3);
    }
}
